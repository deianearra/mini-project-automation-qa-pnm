<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Gender                                 _5336a6</name>
   <tag></tag>
   <elementGuidId>4d3d0d12-c729-4fe2-9198-ea53b786915e</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Your Personal Details'])[1]/following::div[1]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.form-fields</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>6b30ecfd-49cf-494a-88d5-015d741f91f7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-fields</value>
      <webElementGuid>6be7e151-c500-4f5c-b928-d728a029f453</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                        
                            Gender:
                            
                                
                                Male
                            
                            
                                
                                Female
                            
                        
                    
                        First name:
                        
                        *
                        
                    
                    
                        Last name:
                        
                        *
                        
                    
                    
                        Email:
                        
                        *
                        
                    
                </value>
      <webElementGuid>986447f9-24d4-430a-8a18-2925c135fc6d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/div[@class=&quot;master-wrapper-page&quot;]/div[@class=&quot;master-wrapper-content&quot;]/div[@class=&quot;master-wrapper-main&quot;]/div[@class=&quot;center-2&quot;]/form[1]/div[@class=&quot;page registration-page&quot;]/div[@class=&quot;page-body&quot;]/div[@class=&quot;fieldset&quot;]/div[@class=&quot;form-fields&quot;]</value>
      <webElementGuid>8bd6bcfe-b59d-43d8-bcb2-48b03518a630</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Your Personal Details'])[1]/following::div[1]</value>
      <webElementGuid>2b47d7f9-f29e-4ff3-b444-adc7632f59fe</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Register'])[2]/following::div[5]</value>
      <webElementGuid>38d45a15-9e23-4eb4-b193-3fa013189c29</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div[2]/div[2]</value>
      <webElementGuid>4e25b548-313d-47bc-a9c4-aa8e589b561b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
                        
                            Gender:
                            
                                
                                Male
                            
                            
                                
                                Female
                            
                        
                    
                        First name:
                        
                        *
                        
                    
                    
                        Last name:
                        
                        *
                        
                    
                    
                        Email:
                        
                        *
                        
                    
                ' or . = '
                        
                            Gender:
                            
                                
                                Male
                            
                            
                                
                                Female
                            
                        
                    
                        First name:
                        
                        *
                        
                    
                    
                        Last name:
                        
                        *
                        
                    
                    
                        Email:
                        
                        *
                        
                    
                ')]</value>
      <webElementGuid>5e843b30-0db8-4cca-bc93-ca3a147133ea</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
